# Changelog

All notable changes to `ch-gallery` will be documented in this file

## 1.0.0

- initial release
- command to install package easily

## 2.0.0

- support logic as core logic (we couldnt publish core plugin changes as an update. we are solving it here)
- publish only config,routes,and views
- have a controller which extends the core plugin controller
- add alias support within the package


