<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <div class="row">
        <div class="col-md-12">
          <a href="javascript:void(0);" data-toggle="modal" data-target="#ChooseGallery"
             class="btn btn-secondary btn-block-sm">{{__('general.Choose Images')}}</a>
        </div>
      </div>
      <div class="row mt-5" id="imagePreviewList">
        @if($selectedImages)
          @foreach($selectedImages as $image)
          <div class="col-md-2">
            <img class="img-thumbnail img-fluid" src="{{$image->path}}" itemprop="thumbnail" height="100%">
          </div>
          @endforeach
          @endif
      </div>

      <div class="modal fade text-left" id="ChooseGallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16"
           style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel16">{{__('general.Choose Images')}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row masonry-grid my-gallery">
                <div class="col-md-12">
                @if(count($images))
                  <select name="{{ ! empty($customName) ? $customName : 'multiImageList' }}[]" class="multi-image-picker" {{ isset($multiplePick) && (bool) $multiplePick === true ?  'multiple="multiple"' : '' }}>
                    @foreach($images as $key => $image)
                      <option {{ (in_array($image->id, $selectedIds)) ? 'selected' : ''}} data-img-src="{{ Gallery::thumbnail($image->path, 200) }}" data-img-title="{{ $image->title }}" value="{{Storage::url($image->path)}}">{{$image->path}}</option>
                    @endforeach
                  </select>
                @else
                  <div class="col-lg-3 col-md-12">
                    <div href="{{ route('admin.CreateFolder') }}" class="card blocks-folder">
                      <div class="card-content">
                        <div class="card-body text-center">
                          <h4 class="card-title">@lang('gallery.noImages')</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
