<?php

namespace Creativehandles\ChGallery\Http\Controllers\PluginsControllers;

use App\Traits\MultilanguageTrait;
use App\Helpers\Slug;
use App\Http\Controllers\Controller;
use Creativehandles\ChGallery\Plugins\Gallery\Gallery as Gallery;
use Creativehandles\ChGallery\Plugins\Gallery\Models\Image as ImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Validator;

class GalleryController extends Controller
{
    use MultilanguageTrait;

    protected $gallery;
    protected $views = 'Admin.Gallery.';

    public function __construct(Request $request)
    {
        $this->setLocaleInRequestHeader($request);

        // Assign blocks plugin into variable
        $this->gallery = new Gallery();
    }

    /**
     * Render blocks index - folders page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $gallery = new Gallery();
        if($keyword = request()->get('keyword',null)){
            $allImages = $gallery->getImagesLike($keyword);
        }else{
            $allImages = $gallery->getImageWithData();
        }

        return view($this->views."index")->with('images', $allImages)->with('keyword',$keyword);
    }

  /**
   *
   */
    public function uploadImage(Request $request)
    {
      $gallery = new Gallery();
      $file = $request->file('filepond')[0];

      // Pretty URL from original image name
      $originalName = $gallery->clearFileName($file->getClientOriginalName());
      $extension = $gallery->getExt($file->getClientOriginalName());
      $slugName = Slug::make_slug($originalName).".".$extension;
      $storedFile = $file->storeAs('public/images', $slugName);

      // Save original image
      $retreivedFile = Storage::path($storedFile);
      $fileName = $this->gallery->clearFileName($storedFile);
      $thumbnailPath = Storage::path('public/thumbnails/');

      // Save thumbnails
      $possibleSizes = [1000, 800, 600, 400, 200];

      foreach($possibleSizes as $size) {
        $image = Image::make($retreivedFile);
        $image->resize($size, null, function ($constraint) {
          $constraint->aspectRatio();
        });

        //create parent files if not exist
          if(!\File::exists($thumbnailPath.$size)) {
              \File::makeDirectory($thumbnailPath.$size, $mode = 0777, true, true);
          }

        $imagePath = $thumbnailPath.$size.'/'.$fileName.'.'.$gallery->getExt($retreivedFile);
        $uploaded = $image->save($imagePath);
      }

      // If any of thumbnails is uploaded, create row in SQL with original path
      if($uploaded){
        $imageRow = new ImageModel();
        $imageRow->image_path = $storedFile;
        $imageTranslation = $this->getTranslation($request, $imageRow);
        $imageTranslation->title = $fileName;
        $imageRow->save();
      }
    }

    public function saveImage(Request $request)
    {
      parse_str($request->data, $imagesData);

      foreach($imagesData["imageTitle"] as $id => $image){
          $request->merge(['form_locale' => $imagesData['form_locale'][$id]]);
          $this->setLocaleInRequestHeader($request);

          $imageModel = new ImageModel();
          $imageModel = $imageModel::whereId($id)->first();
          $this->fillTranslations($request, $imageModel, ["title" => $image, "alt" => $imagesData["imageAlt"][$id]]);
          $imageModel->save();
      }

        return response()->json(['msg' => ["Updated"]], 200);
    }

    public function deleteImage(Request $request)
    {

        $imageId = $request->get('img_id',null);

        //delete image
        $image = ImageModel::where('id',$imageId)->first();

        $imagepath = $image->image_path;

        $possibleSizes = [1000, 800, 600, 400, 200];

        //delete original image from the storage
        \File::delete(Storage::path('public/images/'.str_replace('public/images/','',$imagepath)));

        foreach ($possibleSizes as $size){
            //delete thumbnails
            \File::delete(Storage::path('public/thumbnails/'.$size.'/'.str_replace('public/images/','',$imagepath)));
        }

        //delete image from storage
        $image->delete();

    }

    public function searchImage(Request $request)
    {
        $keyword = $request->input('keyword');
        $gallery = new Gallery();

        $allImages = $gallery->getImagesLike($keyword);

        $images = array_map(function ($image) {
            //$image->id = $image->id;
            $image->text = $image->title.' [ alt - '.$image->alt.' ]';

            return collect($image)
                ->only(['id', 'path', 'text','title','alt'])
                ->all();
        }, $allImages);

        return response()->json($images);
    }

    public function editImage(Request $request)
    {
        $imageId = $request->get('image_id');
        $language = $request->get('language');

        $imageModel = new ImageModel();
        $image = $imageModel::whereId($imageId)->first();

        if ($image === null) {
            return response()->json(['msg' => ['Image is not found.']], 404);
        }

        $image->setDefaultLocale($language);

        return response()->json(['locale' => $image->locale, 'title' => $image->title, 'alt' => $image->alt]);
    }
}
