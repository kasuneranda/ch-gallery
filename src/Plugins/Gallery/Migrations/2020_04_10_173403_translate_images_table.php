<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TranslateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        if (! Schema::hasTable('image_translations')) {
            Schema::create('image_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('image_id')->unsigned();
                $table->string('locale', 10)->index();
                $table->string('title', 255)->default(null)->nullable();
                $table->string('alt', 255)->default(null)->nullable();
                $table->timestamps();

                $table->unique(['image_id', 'locale']);
                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            });
        }

        // move the existing data into the translations table
        DB::statement(
            'insert into image_translations (image_id, locale, title, alt) select id, :locale, title, alt from images',
            ['locale' => config('app.locale')])
        ;

        // we drop the translation attributes in our main table:
        Schema::table('images', function ($table) {
            $table->dropColumn('title');
            $table->dropColumn('alt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // add the translatable attributes back in to main table
        Schema::table('images', function ($table) {
            $table->string('title', 255)->default(null)->nullable()->after('image_path');
            $table->string('alt', 255)->default(null)->nullable()->after('title');
        });

        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update images as i join image_translations as it on i.id = it.image_id set i.title = it.title, i.alt = it.alt where it.locale = :locale',
            ['locale' => config('app.locale')])
        ;

        Schema::dropIfExists('image_translations');
    }
}
